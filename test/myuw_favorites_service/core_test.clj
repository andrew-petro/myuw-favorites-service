(ns myuw-favorites-service.core-test
  (:require [clojure.test :refer :all]
            [myuw-favorites-service.core :refer :all]))

(deftest hello-world-test
  (testing "The hello world of tests."
    (is
      (= 1 1)
    )
  )
)

(deftest add-favorite-test
  (testing "Adds entry when no entry exists for eppn"
    (is
      (=
        {"bucky" #{"new_favorite"}}
        (add-favorite "bucky" "new_favorite" {})
      )
    )
  )
  (testing "Adds to existing entry when entry exists"
    (is
      (=
        {"bucky" #{"old_favorite" "new_favorite"}}
        (add-favorite "bucky" "new_favorite" {"bucky" #{"old_favorite"}})
      )
    )
  )
  (testing "Favoriting an existing favorite is a no-op"
    (is
      (=
        {"bucky" #{"very-favored"}}
        (add-favorite "bucky" "very-favored" {"bucky" #{"very-favored"}})
      )
    )
  )
  (testing "Handles case where entry exists with empty set of favorites"
    (is
      (=
        {"bucky" #{"new_favorite"}}
        (add-favorite "bucky" "new_favorite" {"bucky" #{}})
      )
    )
  )
)

(deftest remove-favorite-test
  (testing "No-op when no entry exists for eppn"
    (is
      (=
        {"freddy" #{"falcon_favorite"}}
        (remove-favorite "bucky" "not_favorite" {"freddy" #{"falcon_favorite"}})
      )
    )
  )
  (testing "No-op when the item isn't that user's favorite"
    (is
      (=
        {"freddy" #{"falcon_favorite"}}
        (remove-favorite "freddy" "bogus_item" {"freddy" #{"falcon_favorite"}})
      )
    )
  )
  (testing "Removes favorite"
    (is
      (=
        {"bucky" #{"here_to_stay"}}
        (remove-favorite "bucky" "passing_fad" {"bucky" #{"here_to_stay" "passing_fad"}})
      )
    )
  )
)

(deftest eppn-from-request-test
  (testing "Parses out eppn when present"
    (is
      (=
        "bucky@wisc.edu"
        (eppn-from-request {:headers {"eppn" "bucky@wisc.edu"}})
      )
    )
  )
  (testing "nil when eppn not present"
    (is
      (=
        nil
        (eppn-from-request {:headers {}})
      )
    )
  )
)
