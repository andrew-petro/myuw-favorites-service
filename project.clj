(defproject maybe "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {
    :name "Apache License, Version 2.0"
    :url "https://www.apache.org/licenses/LICENSE-2.0.html"
    :distribution :repo
  }
  :dependencies [
    [org.clojure/clojure "1.10.1"]
    [org.clojure/data.json "1.0.0"]
    [ring "1.8.2"]
    [compojure "1.6.2"]]
  :javac-options ["-target" "1.8"]
  :main ^:skip-aot myuw-favorites-service.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all
                       :jvm-opts ["-Dclojure.compiler.direct-linking=true"]}}
  :plugins [
    [lein-ring "0.12.5"]
  ]
  :ring {:handler myuw-favorites-service.core/handler}
)
