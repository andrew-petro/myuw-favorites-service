# myuw-favorites-service

EXPERIMENTAL microservice tracking which MyUW items are a user's favorites.

Currently implemented in-memory as a demonstration.
A real implementation would use a database.

## Kick the tires

`lein ring server`

This application REQUIRES the `eppn` header to be set with a (possibly entirely fictional) eppn.
In real server environments the Shibboleth SP sets this header with the eppn as read from the SAML assertion.
In your local development environment, you might set this header using a browser plugin.

<http://localhost:3000/add/some-id> favorites the item with id "some-id"

<http://localhost:3000/remove/some-id> un-favorites the item with id "some-id"

<http://localhost:3000/view> responds with JSON representing the user's current favorites, as does both the add and remove paths.

## TODO

Currently the add and remove requests are GET requests. This is bad practice, but it makes for a simple demo.

This in-memory implementation is silly. Instead use MyUW's database.

Sort the favorites alphabetically *by the title* of the item. The title is not apparent from the id. So this might need a side lookup for sorting.
