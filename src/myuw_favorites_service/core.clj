(ns myuw-favorites-service.core
  (:gen-class)
  (:require
    [clojure.string :as string]
    [clojure.data.json :as json]
    [compojure.core :refer :all]
    [compojure.route :as route]))

(def in-memory-favorites-map-atom
  (atom {})
)

(defn add-favorite
  "Given a map from eppns to sets of favorites, returns that map with the given favorite added for the given eppn."
  [eppn favorite favorites-map]
  (if (favorites-map eppn)
    (assoc favorites-map eppn (conj (favorites-map eppn) favorite))
    (assoc favorites-map eppn #{ favorite } )
  )
)

(defn remove-favorite
  "Given a map from eppns to sets of favorites, returns that map with the given favorite removed for the given eppn."
  [eppn no-longer-favorite favorites-map]
  (if (favorites-map eppn)
    (assoc favorites-map eppn (disj (favorites-map eppn) no-longer-favorite))
    favorites-map
  )
)

(defn favorites-set-to-vector
  "Converts a set of favorites to a vector suitable for rendering as JSON"
  [favorites-set]
  (vec favorites-set)
  ; TODO: alpha sort the vector
)

(defn view-favorites-in-memory-impl
  "Looks up the given eppn's favorites in the in-memory state implementation"
  [eppn]
  (@in-memory-favorites-map-atom eppn)
)

(defn add-favorite-in-memory-impl!
  "In-memory-state implementation of adding a favorite"
  [eppn new-favorite]
  (swap! in-memory-favorites-map-atom (partial add-favorite eppn new-favorite))
)

(defn remove-favorite-in-memory-impl!
  "In-memory-state implementation of removing a favorite"
  [eppn no-longer-favored]
  (swap! in-memory-favorites-map-atom (partial remove-favorite eppn no-longer-favored))
)

;; TODO: eppn-from-request is a good candidate for abtraction as Ring middleware
(defn eppn-from-request
  "Given a Ring request, returns the value of the eppn header"
  [request]
  ((:headers request) "eppn")
)

(defn handle-view [request]
  {
    :status 200
    :headers {"Content-Type" "text/json"}
    :body
    (json/write-str {
      :favorites
      (favorites-set-to-vector (view-favorites-in-memory-impl (eppn-from-request request)))
    })
  }
)

(defn handle-add
  "Add the given favorite"
  [request new-favorite]
  (do
    (add-favorite-in-memory-impl! (eppn-from-request request) new-favorite)
    (handle-view request)
  )
)

(defn handle-remove
  "Remove the given favorite"
  [request no-longer-favorite]
  (do
    (remove-favorite-in-memory-impl! (eppn-from-request request) no-longer-favorite)
    (handle-view request)
  )
)

(defroutes handler
  (GET "/view" request (handle-view request))
  ;; TODO GET method for requests that mutate state is bad practice
  (GET "/add/:new-favorite" [new-favorite :as request] (handle-add request new-favorite))
  ;; TODO GET method for requests that mutate state is bad practice
  (GET "/remove/:no-longer-favored" [no-longer-favored :as request] (handle-remove request no-longer-favored))
)

(defn -main
  "Main method exists just as another way to verify program compiles."
  [& args]
  (println "This is my favorite world!"))


